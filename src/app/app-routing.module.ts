import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationPageComponent } from './models/authorization-page/authorization-page.component';
import { HomePageComponent } from './models/home-page/home-page.component';
import { NotFoundPageComponent } from './models/not-found-page/not-found-page.component';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
  const routes: Routes = [
    { path: 'log-in', component: AuthorizationPageComponent },
    { path: 'home', component: HomePageComponent },
    { path: '',   redirectTo: '/log-in', pathMatch: 'full' },
    { path: '**', component: NotFoundPageComponent }
  ];
}
